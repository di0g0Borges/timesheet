var employeeInfo = getQueryString();

function getQueryString() {
    var query = {};
    const url = decodeURI(window.location.search).substring(1);
    const keyValues = url.split('&');

    for (var i in keyValues) {
        var key = keyValues[i].split('=');
        if (key.length > 1) {
            query[key[0]] = key[1];
        }
    }
    return query;
}

window.onload = function () {
    let xhttp = new XMLHttpRequest();
    xhttp.overrideMimeType("application/json");

    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            const timeEntries = JSON.parse(this.responseText);
            const employee = timeEntries.find(entry => entry.pis_number === employeeInfo.id);
            showEmployeeHistoric(employee);
        }
    };
    xhttp.open("GET", "./db/timeclock_entries.json", true);
    xhttp.send();
}


function showEmployeeHistoric(employee) {
    employee.entries = groupDates(employee.entries);
    employee.totalBalance = 0;
    const historic = document.getElementById("historic");
    const title = document.getElementById("title");
    title.innerHTML += `<h2>${employeeInfo.name}</h2>`
    historic.innerHTML += `
    <div>
        <div id="header"><h1>PIS Number: ${employeeInfo.id}</h1></div>
        <div class="historic-row">
            <div class="historic-col title">Date</div>
            <div class="historic-col right title">Balance</div>
        </div>
    </div>`
    employee.entries.forEach(date => {
        const timeBalance = { workload: 0, interval: 0 }
        for (let i = 0; i < date.times.length; i++) {
            const duration = moment.duration(moment(date.times[i + 1]).diff(date.times[i], 'minutes'));

            if (i % 2 === 0 && i !== date.times.length - 1) {
                timeBalance.workload += duration
            }
            if (i % 2 !== 0 && i !== date.times.length - 1) {
                timeBalance.interval += duration
            }
        }

        date.timeBalance = timeBalance;
        date.balanceByDay = parseInt(employeeInfo.interval) > timeBalance.interval ?
            (parseInt(employeeInfo.interval) - timeBalance.interval) + (timeBalance.workload - parseInt(employeeInfo.workload)) :
            (timeBalance.workload - parseInt(employeeInfo.workload));

        employee.totalBalance += date.balanceByDay;

        historic.innerHTML += `
        <div class="historic-row">
            <div class="historic-col data">${moment(date.day).format('DD-MM-YYYY')}</div>
            <div class="historic-col right data ${date.balanceByDay < 0 ? 'negative' : ''}">${formatBalance(date.balanceByDay)}</div>
        </div>`
    })
    const header = document.getElementById("header");
    header.innerHTML += `<h3>Total Balance: ${formatBalance(employee.totalBalance)}</h3>`
}

function groupDates(entries) {
    const groups = _.groupBy(entries, function (date) {
        return moment(date).startOf('day').format();
    });

    const result = _.map(groups, function (group, day) {
        return {
            day: day,
            times: _.sortBy(group, function (value) { return moment(value).format() })
        }
    });

    return result
}

function formatBalance(minutes) {
    return minutes < 0 ? `-${Math.floor(minutes * -1 / 60)}:${(minutes * -1 % 60)}` : `${Math.floor(minutes / 60)}:${(minutes % 60)}`
}