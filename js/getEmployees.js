window.onload = function () {

    let xhttp = new XMLHttpRequest();
    xhttp.overrideMimeType("application/json");

    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            const data = JSON.parse(this.responseText);
            showEmployees(data.employees);
        }
    };
    xhttp.open("GET", "./db/config.json", true);
    xhttp.send();
}

function showEmployees(employees) {
    let list = document.getElementById("employees");

    employees.forEach(function (item) {
        list.innerHTML += `<tr class="post" id="primeiro-post">
        <td colspan="1">${item.pis_number}</td>
        <td colspan="5">${item.name}</td>
        <td colspan="1">${item.workload[0].days[0]} - ${item.workload[0].days[item.workload[0].days.length - 1]}</td>
        <td colspan="1">${item.workload[0].minimum_rest_interval_in_minutes}</td>
        <td colspan="1">${item.workload[0].workload_in_minutes}</td>
        <th colspan="1">
            <a href="./historic.html?id=${item.pis_number}&name=${item.name}&interval=${item.workload[0].minimum_rest_interval_in_minutes}&workload=${item.workload[0].workload_in_minutes}">
                <i class="fa fa-file-text-o"></i>
            </a>
        </th>
        </tr>`;
    })
}