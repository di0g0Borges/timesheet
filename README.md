## Timesheet Extreme Digital Solutions

Este projeto simula requisições HTTP utilizando como base dois arquivos '.json', portanto o mesmo deve ser configurado em um servidor local (http://localhost:xxxx) para que não hajam conflitos no browser.

Após clonar o projeto, para iniciá-lo siga os seguintes passos:

## Windows

1. Instale o NPM (Node Package Manager).
2. Abra o terminal e instale o pacote http-server com o seguinte comando:
    npm install -g http-server.
3. Encontre o caminho da pasta onde o projeto foi clonado e rode o comando:
    http-server
4. Utilize um dos endereços que serão exibidos para iniciar o projeto.

---

## Linux (Testado no Ubuntu 16.04)

O Python geralmente está disponível na maioria das distribuições de linux.

1. Encontre o caminho da pasta onde o projeto foi clonado e rode o comando:
    python -m SimpleHTTPServer
2. Inicie o projeto através do endereço **http://localhost:8000**.

---

por: Diogo Borges